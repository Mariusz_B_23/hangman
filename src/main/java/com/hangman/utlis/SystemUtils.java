package com.hangman.utlis;

import com.hangman.gui.MainVboxView;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicInteger;

public class SystemUtils {

    public static Scanner scanner;
    public static final Random RANDOM = new Random();
    public static final String GAME_NAME = "WISIELEC";
    public static final String FILE_PATH_WITH_PASSWORDS = ("Hangman.txt");
    public static final String FILE_PATH_WITH_SMILE = "gameplay_image/smile.png";
    public static final String ALPHABET_AS_STRING = "AĄBCĆDEĘFGHIJKLŁMNŃOÓPQRSŚTUVWXYZŹŻ";
    public static final int INITIAL_VALUE = 0;
    private static final int SMILE_BUTTON_PREF_SIZE = 50;
    public static final String SINGLE_GAME_MODE = "JEDEN GRACZ";
    public static final String MULTI_GAME_MODE = "DWÓCH GRACZY";
    public static final String HARD_GAME_MODE = "DO 3 RAZY SZTUKA";


    public static Button createBackToMenuButton(VBox vBox, Stage primaryStage) {
        Button backButton = new Button("POWRÓT DO MENU");
        backButton.setPrefSize(150, 30);
        backButton.setStyle("-fx-background-radius: 3em;");
        backButton.setFont(Font.font("Arial", FontWeight.BOLD, 12));
        backButton.setLayoutX(10);
        backButton.setLayoutY(10);
        backButton.setOnAction(event -> {
            vBox.getChildren().clear();
            MainVboxView mainVboxView = new MainVboxView(primaryStage);
            vBox.getChildren().add(mainVboxView.getMainBoxView());
            vBox.setAlignment(Pos.CENTER);
        });
        return backButton;
    }

    public static Button createContinueButton(VBox vBox, Stage primaryStage) {
        Button continueButton = new Button("KONTYNUUJ");
        continueButton.setPrefSize(150, 30);
        continueButton.setStyle("-fx-background-radius: 3em;");
        continueButton.setFont(Font.font("Arial", FontWeight.BOLD, 12));
        continueButton.setLayoutX(primaryStage.getWidth() - continueButton.getPrefWidth() - 50);
        continueButton.setLayoutY(primaryStage.getHeight() - continueButton.getHeight() - 50);
        continueButton.setOnAction(event -> vBox.getChildren().clear());
        return continueButton;
    }

    public static String getPasswordFromList(List<String> words) {
        if (words.isEmpty()) {
            throw new IllegalArgumentException("Lista wyrazów nie może być pusta!");
        }
        return words.get(RANDOM.nextInt(words.size()));
    }

    public static List<String> downloadDataFile() {
        InputStream inputStream = SystemUtils.class.getClassLoader().getResourceAsStream(FILE_PATH_WITH_PASSWORDS);
        if (inputStream == null) {
            throw new RuntimeException("File not found: " + FILE_PATH_WITH_PASSWORDS);
        }
        scanner = new Scanner(inputStream);
        List<String> words = new ArrayList<>();
        while (scanner.hasNext()) {
            words.add(scanner.nextLine());
        }
        return words;
    }


    public static Button createGameButton(String gameName) {
        Button singlePlayerButton = new Button(gameName);
        singlePlayerButton.setPrefSize(400, 50);
        singlePlayerButton.setStyle("-fx-background-radius: 3em;");
        singlePlayerButton.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        return singlePlayerButton;
    }

    public static Label welcomeMessageLabel(String welcomeMessage) {
        Label labelWithWelcomeMessage = new Label(welcomeMessage);
        labelWithWelcomeMessage.setFont(Font.font("Ubuntu", FontWeight.BOLD, 40));
        labelWithWelcomeMessage.setPadding(new Insets(100, 100, 100, 100));
        labelWithWelcomeMessage.setLayoutX(50);
        labelWithWelcomeMessage.setLayoutY(50);
        labelWithWelcomeMessage.setTextAlignment(TextAlignment.CENTER);
        labelWithWelcomeMessage.setAlignment(Pos.CENTER);
        labelWithWelcomeMessage.setWrapText(true);
        labelWithWelcomeMessage.setOpacity(0);
        return labelWithWelcomeMessage;
    }

    public static Pane createGamePane(VBox vBox, Stage primaryStage,
                                      String winnerGifPath, int winnerGifSetFitWidth,
                                      int winnerGifSetLayoutX, int winnerGifSetLayoutY,
                                      int penultimateTry, int maximumAttempts,
                                      String partOfImagePath, String loseGifPath,
                                      int loseGifSetFitWidth, int loseGifSetLayoutX,
                                      int loseGifSetLayoutY, int setFitWidthPartOfImage,
                                      int setLayoutXPartOfImage, int setLayoutYPartOfImage,
                                      String finalImagePath, int setFitWidthFinalImage,
                                      int setLayoutXFinalImage, int setLayoutYFinalImage) {
        Pane gamePane = new Pane();
        Label hangmanLogo = createHangmanLogo();
        vBox.getChildren().clear();
        vBox.getChildren().add(gamePane);
        gamePane.setPrefSize(primaryStage.getWidth(), primaryStage.getHeight());
        AnchorPane passwordAnchorPane = createPasswordAnchorPane(vBox, primaryStage,
                winnerGifPath, winnerGifSetFitWidth, winnerGifSetLayoutX, winnerGifSetLayoutY,
                penultimateTry, maximumAttempts,
                partOfImagePath, loseGifPath, loseGifSetFitWidth, loseGifSetLayoutX,
                loseGifSetLayoutY, setFitWidthPartOfImage, setLayoutXPartOfImage, setLayoutYPartOfImage,
                finalImagePath, setFitWidthFinalImage, setLayoutXFinalImage, setLayoutYFinalImage);
        gamePane.getChildren().addAll(hangmanLogo, passwordAnchorPane);
        return gamePane;
    }

    public static Pane createGamePane(VBox vBox, Stage primaryStage, String password,
                                      String winnerGifPath, int winnerGifSetFitWidth,
                                      int winnerGifSetLayoutX, int winnerGifSetLayoutY,
                                      int penultimateTry, int maximumAttempts,
                                      String partOfImagePath, String loseGifPath,
                                      int loseGifSetFitWidth, int loseGifSetLayoutX,
                                      int loseGifSetLayoutY, int setFitWidthPartOfImage,
                                      int setLayoutXPartOfImage, int setLayoutYPartOfImage,
                                      String finalImagePath, int setFitWidthFinalImage,
                                      int setLayoutXFinalImage, int setLayoutYFinalImage) {
        Pane gamePane = new Pane();
        Label hangmanLogo = SystemUtils.createHangmanLogo();
        vBox.getChildren().clear();
        vBox.getChildren().add(gamePane);
        gamePane.setPrefSize(primaryStage.getWidth(), primaryStage.getHeight());
        AnchorPane passwordAnchorPane = createPasswordAnchorPane(vBox, primaryStage, password,
                winnerGifPath, winnerGifSetFitWidth, winnerGifSetLayoutX, winnerGifSetLayoutY,
                penultimateTry, maximumAttempts,
                partOfImagePath, loseGifPath, loseGifSetFitWidth, loseGifSetLayoutX,
                loseGifSetLayoutY, setFitWidthPartOfImage, setLayoutXPartOfImage, setLayoutYPartOfImage,
                finalImagePath, setFitWidthFinalImage, setLayoutXFinalImage, setLayoutYFinalImage);
        gamePane.getChildren().addAll(hangmanLogo, passwordAnchorPane);
        return gamePane;
    }

    public static Label createHangmanLogo() {
        Label logoLabel = new Label(SystemUtils.GAME_NAME);
        logoLabel.setFont(Font.font("Verdana", FontWeight.BOLD, FontPosture.ITALIC, 78));
        logoLabel.setLayoutX(80);
        logoLabel.setLayoutY(50);
        logoLabel.setPadding(new Insets(20, 20, 20, 20));
        return logoLabel;
    }

    private static AnchorPane createPasswordAnchorPane(VBox vBox, Stage primaryStage,
                                                       String winnerGifPath, int winnerGifSetFitWidth,
                                                       int winnerGifSetLayoutX, int winnerGifSetLayoutY,
                                                       int penultimateTry, int maximumAttempts,
                                                       String partOfImagePath, String loseGifPath,
                                                       int loseGifSetFitWidth, int loseGifSetLayoutX,
                                                       int loseGifSetLayoutY, int setFitWidthPartOfImage,
                                                       int setLayoutXPartOfImage, int setLayoutYPartOfImage,
                                                       String finalImagePath, int setFitWidthFinalImage,
                                                       int setLayoutXFinalImage, int setLayoutYFinalImage) {
        AnchorPane passwordAnchorPane = new AnchorPane();
        String password = getPasswordFromList(SystemUtils.downloadDataFile());
        System.out.println("Hasło ->   " + password);
        Label passwordInfoLabel = createPasswordInfoLabel();
        HBox passwordHBox = createPasswordHBox(password);
        Button backButtonInGamePane = createBackToMenuButton(vBox, primaryStage);
        GridPane alphabetPane = createAlphabetGridPane(password, passwordHBox, backButtonInGamePane,
                passwordAnchorPane, penultimateTry, maximumAttempts, partOfImagePath, loseGifPath, loseGifSetFitWidth,
                loseGifSetLayoutX, loseGifSetLayoutY, setFitWidthPartOfImage, setLayoutXPartOfImage, setLayoutYPartOfImage,
                finalImagePath, setFitWidthFinalImage, setLayoutXFinalImage, setLayoutYFinalImage,
                winnerGifPath, winnerGifSetFitWidth, winnerGifSetLayoutX, winnerGifSetLayoutY);
        VBox passwordProposition = passwordPropositionVBox(password, backButtonInGamePane, passwordAnchorPane,
                winnerGifPath, winnerGifSetFitWidth, winnerGifSetLayoutX, winnerGifSetLayoutY);
        passwordAnchorPane.getChildren()
                .addAll(passwordInfoLabel, passwordHBox, backButtonInGamePane, alphabetPane,
                        passwordProposition);
        return passwordAnchorPane;
    }

    private static AnchorPane createPasswordAnchorPane(VBox vBox, Stage primaryStage, String password,
                                                       String winnerGifPath, int winnerGifSetFitWidth,
                                                       int winnerGifSetLayoutX, int winnerGifSetLayoutY,
                                                       int penultimateTry, int maximumAttempts,
                                                       String partOfImagePath, String loseGifPath,
                                                       int loseGifSetFitWidth, int loseGifSetLayoutX,
                                                       int loseGifSetLayoutY, int setFitWidthPartOfImage,
                                                       int setLayoutXPartOfImage, int setLayoutYPartOfImage,
                                                       String finalImagePath, int setFitWidthFinalImage,
                                                       int setLayoutXFinalImage, int setLayoutYFinalImage) {
        AnchorPane passwordAnchorPane = new AnchorPane();
        System.out.println("Hasło ->   " + password);
        Label passwordInfoLabel = createPasswordInfoLabel();
        HBox passwordHBox = createPasswordHBox(password);
        Button backButtonInGamePane = SystemUtils.createBackToMenuButton(vBox, primaryStage);
        GridPane alphabetPane = createAlphabetGridPane(password, passwordHBox, backButtonInGamePane,
                passwordAnchorPane, penultimateTry, maximumAttempts, partOfImagePath, loseGifPath, loseGifSetFitWidth,
                loseGifSetLayoutX, loseGifSetLayoutY, setFitWidthPartOfImage, setLayoutXPartOfImage, setLayoutYPartOfImage,
                finalImagePath, setFitWidthFinalImage, setLayoutXFinalImage, setLayoutYFinalImage,
                winnerGifPath, winnerGifSetFitWidth, winnerGifSetLayoutX, winnerGifSetLayoutY);
        VBox passwordProposition = passwordPropositionVBox(password, backButtonInGamePane, passwordAnchorPane,
                winnerGifPath, winnerGifSetFitWidth, winnerGifSetLayoutX, winnerGifSetLayoutY);
        passwordAnchorPane.getChildren()
                .addAll(passwordInfoLabel, passwordHBox, backButtonInGamePane, alphabetPane,
                        passwordProposition);
        return passwordAnchorPane;
    }

    private static VBox passwordPropositionVBox(String password, Button backButton, AnchorPane anchorPane,
                                                String winnerGifPath, int winnerGifSetFitWidth,
                                                int winnerGifSetLayoutX, int winnerGifSetLayoutY) {
        VBox passwordPropositionVbox = createPasswordPropositionVbox();
        Label passwordTextLabel = createPasswordTextLabel();
        TextField passwordPropositionTextLabel = createPasswordPropositionTextLabel();
        Button confirmButton = createConfirmButton();
        confirmButton.setOnAction(event -> {
            String proposedPassword = passwordPropositionTextLabel.getText().toUpperCase();
            if (proposedPassword.equals(password.toUpperCase())) {
                createCongratulationsPane
                        (backButton, anchorPane, password, winnerGifPath,
                                winnerGifSetFitWidth, winnerGifSetLayoutX, winnerGifSetLayoutY);
            } else {
                createIncorrectPasswordPropositionLabel(passwordPropositionVbox);
            }
        });
        passwordPropositionVbox.getChildren().addAll(passwordTextLabel, passwordPropositionTextLabel, confirmButton);
        return passwordPropositionVbox;
    }

    private static VBox createPasswordPropositionVbox() {
        VBox passwordPropositionVbox = new VBox();
        passwordPropositionVbox.setLayoutX(100);
        passwordPropositionVbox.setLayoutY(325);
        passwordPropositionVbox.setPrefSize(450, 100);
        return passwordPropositionVbox;
    }

    private static Label createPasswordTextLabel() {
        Label passwordTextLabel = new Label("PODAJ SWOJĄ PROPOZYCJĘ HASŁA");
        passwordTextLabel.setStyle("-fx-border-color: black; -fx-border-width: 2px;");
        passwordTextLabel.setPadding(new Insets(5, 10, 5, 10));
        passwordTextLabel.setFont(Font.font("Comic Sans MS", FontWeight.BOLD, 20));
        passwordTextLabel.setPrefSize(450, 50);
        return passwordTextLabel;
    }

    private static TextField createPasswordPropositionTextLabel() {
        TextField passwordPropositionTextLabel = new TextField();
        passwordPropositionTextLabel.setStyle("-fx-border-color: black; -fx-border-width: 2px;");
        passwordPropositionTextLabel.setPadding(new Insets(5, 10, 5, 10));
        passwordPropositionTextLabel.setFont(Font.font("Comic Sans MS", FontWeight.BOLD, 20));
        passwordPropositionTextLabel.setPrefSize(450, 50);
        return passwordPropositionTextLabel;
    }

    private static Button createConfirmButton() {
        Button confirmButton = new Button("SPRAWDŹ PROPOZYCJĘ HASŁA");
        confirmButton.setPrefSize(450, 100);
        confirmButton.setStyle("-fx-background-radius: 5em;");
        confirmButton.setFont(Font.font("Comic Sans MS", FontWeight.BOLD, 15));
        return confirmButton;
    }

    private static void createIncorrectPasswordPropositionLabel(VBox passwordProposition) {
        Label incorrectPasswordPropositionLabel = new Label("HASŁO JEST NIEPRAWIDŁOWE");
        incorrectPasswordPropositionLabel.setTextFill(Color.RED);
        incorrectPasswordPropositionLabel.setFont(Font.font("Comic Sans MS", FontWeight.BOLD, 15));
        passwordProposition.getChildren().add(incorrectPasswordPropositionLabel);
        Timeline timeline = new Timeline(new KeyFrame(Duration.seconds(2),
                e -> passwordProposition.getChildren().remove(incorrectPasswordPropositionLabel)));
        timeline.play();
    }

    private static HBox createPasswordHBox(String password) {
        String hiddenPassword = password.replaceAll("[^ ]", "*");
        Label passwordLabel = createPasswordLabel();
        Label hiddenPasswordLabel = createHiddenPasswordLabel(hiddenPassword);
        HBox passwordHBox = new HBox(passwordLabel, hiddenPasswordLabel);
        passwordHBox.setLayoutX(520);
        passwordHBox.setLayoutY(700);
        passwordHBox.setStyle("-fx-border-color: black; -fx-border-width: 2px;");
        passwordHBox.setPadding(new Insets(5, 10, 5, 10));
        return passwordHBox;
    }

    private static Label createHiddenPasswordLabel(String hiddenPassword) {
        Label hiddenPasswordLabel = new Label(hiddenPassword);
        hiddenPasswordLabel.setFont(Font.font("Comic Sans MS", FontWeight.BOLD, 35));
        return hiddenPasswordLabel;
    }

    private static Label createPasswordLabel() {
        Label passwordLabel = new Label("HASŁO:   ");
        passwordLabel.setFont(Font.font("Comic Sans MS", FontWeight.BOLD, 35));
        return passwordLabel;
    }

    private static Label createPasswordInfoLabel() {
        Label passwordInfoLabel = new Label("WYBIERZ LITERĘ I SPRÓBUJ ODGADNĄĆ HASŁO");
        passwordInfoLabel.setFont(Font.font("Comic Sans MS", FontWeight.BOLD, 35));
        passwordInfoLabel.setLayoutX(225);
        passwordInfoLabel.setLayoutY(475);
        passwordInfoLabel.setStyle("-fx-border-color: black; -fx-border-width: 2px;");
        passwordInfoLabel.setPadding(new Insets(5, 10, 5, 10));
        return passwordInfoLabel;
    }

    private static GridPane createAlphabetGridPane(String password, HBox passwordHbox, Button backButton,
                                                   AnchorPane passwordAnchorPane, int penultimateTry,
                                                   int maximumAttempts, String partOfImagePath, String loseGifPath,
                                                   int loseGifSetFitWidth, int loseGifSetLayoutX,
                                                   int loseGifSetLayoutY, int setFitWidthPartOfImage,
                                                   int setLayoutXPartOfImage, int setLayoutYPartOfImage,
                                                   String finalImagePath, int setFitWidthFinalImage,
                                                   int setLayoutXFinalImage, int setLayoutYFinalImage,
                                                   String winnerGifPath, int winnerGifSetFitWidth,
                                                   int winnerGifSetLayoutX, int winnerGifSetLayoutY) {
        GridPane alphabetPane = getAlphabetPane();
        char[] alphabet = (ALPHABET_AS_STRING).toCharArray();
        char[] passwordChar = password.toCharArray();
        Label hiddenPasswordLabel = (Label) passwordHbox.getChildren().get(1);
        AtomicInteger wrongAttempts = new AtomicInteger(INITIAL_VALUE);
        AtomicInteger remainingAttempts = new AtomicInteger(maximumAttempts);
        Label wrongAttemptsLabel = createWrongAttemptsLabel(wrongAttempts);
        Label remainingAttemptsLabel = createRemainingAttemptsLabel(remainingAttempts);
        Label passwordInfoLabel = createPasswordInfoLabel();
        passwordAnchorPane.getChildren()
                .addAll(wrongAttemptsLabel, remainingAttemptsLabel, passwordInfoLabel);
        Button smileButton = SystemUtils.createSmileButton();
        alphabetPane.add(smileButton, 0, 0);
        if (wrongAttempts.get() == 0) {
            ImageView firstImage = showPartOfImage(wrongAttempts.get(), partOfImagePath, setFitWidthPartOfImage,
                    setLayoutXPartOfImage, setLayoutYPartOfImage);
            passwordAnchorPane.getChildren().add(firstImage);
        }
        for (int i = 0; i < alphabet.length; i++) {
            char letter = alphabet[i];
            Button letterButton = createLetterButton(letter, event -> {
                Button clickedButton = (Button) event.getSource();
                if (clickedButton.isDisabled()) {
                    return;
                }
                clickedButton.setStyle("-fx-background-color: #8B0000; -fx-background-radius: 2em; -fx-padding: 5px;");
                clickedButton.setDisable(true);
                boolean found = false;
                for (int j = 0; j < passwordChar.length; j++) {
                    if (passwordChar[j] == letter) {
                        hiddenPasswordLabel.setText(hiddenPasswordLabel.getText().substring(0, j) +
                                letter + hiddenPasswordLabel.getText().substring(j + 1));
                        found = true;
                    }
                }
                if (!found) {
                    wrongAttempts.getAndIncrement();
                    remainingAttempts.getAndDecrement();
                    wrongAttemptsLabel.setText("Niepoprawne próby: " + wrongAttempts.get());
                    remainingAttemptsLabel.setText("Pozostało prób:      " + remainingAttempts.get());
                    if (maximumAttempts == wrongAttempts.get()
                            && !(password.equals(hiddenPasswordLabel.getText()))) {
                        createLosePane(backButton, passwordAnchorPane, password, loseGifPath,
                                loseGifSetFitWidth, loseGifSetLayoutX, loseGifSetLayoutY,
                                finalImagePath, setFitWidthFinalImage, setLayoutXFinalImage, setLayoutYFinalImage);
                    }
                    if (wrongAttempts.get() <= penultimateTry) {
                        ImageView image = showPartOfImage(wrongAttempts.get(),
                                partOfImagePath, setFitWidthPartOfImage, setLayoutXPartOfImage, setLayoutYPartOfImage);
                        passwordAnchorPane.getChildren().add(image);
                    }
                }
                if (password.equals(hiddenPasswordLabel.getText())) {
                    createCongratulationsPane(backButton, passwordAnchorPane, password,
                            winnerGifPath, winnerGifSetFitWidth, winnerGifSetLayoutX, winnerGifSetLayoutY);
                }
            });
            alphabetPane.add(letterButton, (i + 1) % 18, (i + 1) / 18);
        }
        return alphabetPane;
    }

    private static GridPane getAlphabetPane() {
        GridPane alphabetPane = new GridPane();
        alphabetPane.setLayoutX(50);
        alphabetPane.setLayoutY(550);
        alphabetPane.setHgap(5);
        alphabetPane.setVgap(5);
        return alphabetPane;
    }

    private static Label createWrongAttemptsLabel(AtomicInteger wrongAttempts) {
        Label wrongAttemptsLabel = new Label();
        wrongAttemptsLabel.setText("Niepoprawne próby: " + wrongAttempts.get());
        wrongAttemptsLabel.setLayoutX(100);
        wrongAttemptsLabel.setLayoutY(200);
        wrongAttemptsLabel.setPrefSize(250, 50);
        wrongAttemptsLabel.setPadding(new Insets(5, 10, 5, 10));
        wrongAttemptsLabel.setStyle("-fx-border-color: black; -fx-border-width: 2px;");
        wrongAttemptsLabel.setFont(Font.font("Comic Sans MS", FontWeight.BOLD, 20));
        return wrongAttemptsLabel;
    }

    private static Label createRemainingAttemptsLabel(AtomicInteger remainingAttempts) {
        Label remainingAttemptsLabel = new Label();
        remainingAttemptsLabel.setText("Pozostało prób:      " + remainingAttempts.get());
        remainingAttemptsLabel.setLayoutX(100);
        remainingAttemptsLabel.setLayoutY(250);
        remainingAttemptsLabel.setPrefSize(250, 50);
        remainingAttemptsLabel.setPadding(new Insets(5, 10, 5, 10));
        remainingAttemptsLabel.setStyle("-fx-border-color: black; -fx-border-width: 2px;");
        remainingAttemptsLabel.setFont(Font.font("Comic Sans MS", FontWeight.BOLD, 20));
        return remainingAttemptsLabel;
    }

    private static Button createLetterButton(char letter, EventHandler<ActionEvent> actionHandler) {
        Button letterButton = new Button(String.valueOf(letter));
        letterButton.setPrefSize(65, 65);
        letterButton.setStyle("-fx-background-color: #4CAF50; -fx-background-radius: 2em;");
        letterButton.setTextFill(Color.BLACK);
        letterButton.setFont(Font.font("Comic Sans MS", FontWeight.BOLD, 20));
        letterButton.setOnAction(actionHandler);
        return letterButton;
    }

    private static void createLosePane(Button backButton, AnchorPane anchorPane, String password, String loseGifPath,
                                       int loseGifSetFitWidth, int loseGifSetLayoutX, int loseGifSetLayoutY,
                                       String finalImagePath, int setFitWidthFinalImage,
                                       int setLayoutXFinalImage, int setLayoutYFinalImage) {
        Pane losePane = new Pane();
        Label loseLabel = createLoseLabel();
        Label passwordLabel = createExitLabelWithPassword(password);
        ImageView imageView = getLoseGif(loseGifPath, loseGifSetFitWidth, loseGifSetLayoutX, loseGifSetLayoutY);
        ImageView finalImage =
                getFinalImage(finalImagePath, setFitWidthFinalImage, setLayoutXFinalImage, setLayoutYFinalImage);
        losePane.getChildren().add(loseLabel);
        anchorPane.getChildren().clear();
        anchorPane.getChildren().addAll(losePane, backButton, imageView, finalImage, passwordLabel);
    }

    private static Label createLoseLabel() {
        Label loseLabel = new Label("NIESTETY PRZEGRAŁEŚ!!!");
        loseLabel.setFont(Font.font("Comic Sans MS", FontWeight.BOLD, 75));
        loseLabel.setTextFill(Color.DARKRED);
        loseLabel.setLayoutX(100);
        loseLabel.setLayoutY(275);
        return loseLabel;
    }

    private static Label createExitLabelWithPassword(String password) {
        Label exitLabelWithPassword = new Label("HASŁO: " + password);
        exitLabelWithPassword.setFont(Font.font("Comic Sans MS", FontWeight.BOLD, 50));
        exitLabelWithPassword.setTextFill(Color.BLUE);
        exitLabelWithPassword.setLayoutX(100);
        exitLabelWithPassword.setLayoutY(190);
        return exitLabelWithPassword;
    }

    private static ImageView getLoseGif
            (String loseGifPath, int loseGifSetFitWidth, int loseGifSetLayoutX, int loseGifSetLayoutY) {
        Image loseImage = new Image(loseGifPath);
        ImageView loseImageView = new ImageView(loseImage);
        loseImageView.setFitWidth(loseGifSetFitWidth);
        loseImageView.setPreserveRatio(true);
        loseImageView.setLayoutX(loseGifSetLayoutX);
        loseImageView.setLayoutY(loseGifSetLayoutY);
        return loseImageView;
    }

    private static ImageView getWinnerGif
            (String winnerGifPath, int winnerGifSetFitWidth, int winnerGifSetLayoutX, int winnerGifSetLayoutY) {
        Image winnerImage = new Image(winnerGifPath);
        ImageView winnerImageView = new ImageView(winnerImage);
        winnerImageView.setFitWidth(winnerGifSetFitWidth);
        winnerImageView.setPreserveRatio(true);
        winnerImageView.setLayoutX(winnerGifSetLayoutX);
        winnerImageView.setLayoutY(winnerGifSetLayoutY);
        return winnerImageView;
    }

    private static void createCongratulationsPane(Button backButton, AnchorPane anchorPane, String password,
                                                  String winnerGifPath, int winnerGifSetFitWidth,
                                                  int winnerGifSetLayoutX, int winnerGifSetLayoutY) {
        Pane congratulationsPane = new Pane();
        Label congratulationsLabel = createCongratulationsLabel();
        Label passwordLabel = createExitLabelWithPassword(password);
        ImageView imageView = getWinnerGif(winnerGifPath, winnerGifSetFitWidth,
                winnerGifSetLayoutX, winnerGifSetLayoutY);
        congratulationsPane.getChildren().add(congratulationsLabel);
        anchorPane.getChildren().clear();
        anchorPane.getChildren().addAll(congratulationsPane, backButton, imageView, passwordLabel);
    }

    private static Label createCongratulationsLabel() {
        Label congratulationsLabel = new Label("BRAWO WYGRAŁEŚ!!!");
        congratulationsLabel.setFont(Font.font("Comic Sans MS", FontWeight.BOLD, 75));
        congratulationsLabel.setTextFill(Color.GREEN);
        congratulationsLabel.setLayoutX(100);
        congratulationsLabel.setLayoutY(275);
        return congratulationsLabel;
    }

    private static ImageView getFinalImage(String finalImagePath, int setFitWidth, int setLayoutX, int setLayouty) {
        Image finalImage = new Image(finalImagePath);
        ImageView imageView = new ImageView(finalImage);
        imageView.setFitWidth(setFitWidth);
        imageView.setPreserveRatio(true);
        imageView.setLayoutX(setLayoutX);
        imageView.setLayoutY(setLayouty);
        return imageView;
    }

    private static ImageView showPartOfImage
            (int wrongAttemptsNumber, String partOfImagePath, int setFitWidthPartOfImage,
             int setLayoutXPartOfImage, int setLayoutYPartOfImage) {
        String image = String.format(partOfImagePath, wrongAttemptsNumber);
        Image partOfImage = new Image(image);
        ImageView imageView = new ImageView(partOfImage);
        imageView.setFitWidth(setFitWidthPartOfImage);
        imageView.setPreserveRatio(true);
        imageView.setLayoutX(setLayoutXPartOfImage);
        imageView.setLayoutY(setLayoutYPartOfImage);
        return imageView;
    }

    public static Button createSmileButton() {
        Button smileButton = new Button();
        smileButton.setPrefSize(SMILE_BUTTON_PREF_SIZE, SMILE_BUTTON_PREF_SIZE);
        ImageView smileImage = new ImageView(new Image(FILE_PATH_WITH_SMILE));
        smileImage.setFitWidth(smileButton.getPrefWidth());
        smileImage.setFitHeight(smileButton.getPrefHeight());
        smileButton.setBackground(null);
        smileButton.setStyle("-fx-background-color: transparent; -fx-background-radius: 2em;");
        smileButton.setGraphic(smileImage);
        return smileButton;
    }

}