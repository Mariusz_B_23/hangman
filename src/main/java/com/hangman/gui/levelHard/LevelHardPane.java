package com.hangman.gui.levelHard;

import com.hangman.gui.MaximumAttempts;
import com.hangman.utlis.SystemUtils;
import javafx.animation.PauseTransition;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Duration;

public class LevelHardPane {

    private final AnchorPane levelHardPane;
    private static final String WINNER_GIF_PATH = "gameplay_image/hard/not today devil.gif";
    private static final int WINNER_GIF_SET_FIT_WIDTH = 500;
    private static final int WINNER_GIF_SET_LAYOUT_X = 400;
    private static final int WINNER_GIF_SET_LAYOUT_Y = 420;
    private static final int PENULTIMATE_TRY = 3;
    private static final int MAXIMUM_ATTEMPTS = MaximumAttempts.LEVEL_HARD.getQuantity();
    private static final String PART_OF_IMAGE_PART = "gameplay_image/hard/%d.jpg";
    private static final int SET_FIT_WIDTH_PART_OF_IMAGE = 600;
    private static final int SET_LAYOUT_X_PART_OF_IMAGE = 700;
    private static final int SET_LAYOUT_Y_PART_OF_IMAGE = 100;
    private static final String LOSE_GIF_PATH = "gameplay_image/hard/smiling skull.gif";
    private static final int LOSE_GIF_SET_FIT_WIDTH = 310;
    private static final int LOSE_GIF_SET_LAYOUT_X = 200;
    private static final int LOSE_GIF_SET_LAYOUT_Y = 420;
    private static final String FINAL_IMAGE_PATH = "gameplay_image/hard/final image level hard.jpg";
    private static final int SET_FIT_WIDTH_FINAL_IMAGE = 630;
    private static final int SET_LAYOUT_X_FINAL_IMAGE = 650;
    private static final int SET_LAYOUT_Y_FINAL_IMAGE = 420;


    public LevelHardPane(Stage primaryStage, VBox vBox) {
        this.levelHardPane = new AnchorPane(createMainAnchorPane(vBox, primaryStage));
    }

    public AnchorPane getLevelHardPane() {
        return levelHardPane;
    }

    private static AnchorPane createMainAnchorPane(VBox vBox, Stage primaryStage) {
        AnchorPane mainAnchorPane = new AnchorPane();
        vBox.getChildren().add(mainAnchorPane);
        mainAnchorPane.setPrefSize(primaryStage.getWidth(), primaryStage.getHeight());
        Button backButtonInInfoPane = SystemUtils.createBackToMenuButton(vBox, primaryStage);
        mainAnchorPane.getChildren().add(backButtonInInfoPane);
        Label labelWithWelcomeMessage = createLabelWithWelcomeMessage();
        mainAnchorPane.getChildren().add(labelWithWelcomeMessage);
        Button continueButton = SystemUtils.createContinueButton(vBox, primaryStage);
        continueButton.setOnAction(actionEvent -> {
            Pane gamePane = SystemUtils.createGamePane(vBox, primaryStage,
                    WINNER_GIF_PATH, WINNER_GIF_SET_FIT_WIDTH,
                    WINNER_GIF_SET_LAYOUT_X, WINNER_GIF_SET_LAYOUT_Y,
                    PENULTIMATE_TRY, MAXIMUM_ATTEMPTS, PART_OF_IMAGE_PART,
                    LOSE_GIF_PATH, LOSE_GIF_SET_FIT_WIDTH,
                    LOSE_GIF_SET_LAYOUT_X, LOSE_GIF_SET_LAYOUT_Y,
                    SET_FIT_WIDTH_PART_OF_IMAGE, SET_LAYOUT_X_PART_OF_IMAGE, SET_LAYOUT_Y_PART_OF_IMAGE,
                    FINAL_IMAGE_PATH, SET_FIT_WIDTH_FINAL_IMAGE,
                    SET_LAYOUT_X_FINAL_IMAGE, SET_LAYOUT_Y_FINAL_IMAGE);
            vBox.getChildren().clear();
            vBox.getChildren().add(gamePane);
        });
        mainAnchorPane.getChildren().add(continueButton);
        return mainAnchorPane;
    }

    private static Label createLabelWithWelcomeMessage() {
        String welcomeMessage = """
                Witaj w grze o specjalnym poziomie trudności.
                                
                Podczas odgadywania słowa możesz pomylić się ...
                ... maksymalnie 3 razy!
                    
                Jeśli pomylisz się 4 raz ...
                ... to zawiśniesz :)
                                
                Czas na wyzwanie!
                """;
        Label labelWithWelcomeMessage = SystemUtils.welcomeMessageLabel(welcomeMessage);
        int welcomeMessageLength = welcomeMessage.length();
        PauseTransition[] transitions = new PauseTransition[welcomeMessageLength];
        for (int i = 0; i < welcomeMessageLength; i++) {
            transitions[i] = new PauseTransition(Duration.seconds(i * 0.06));
            final int finalI = i + 1;
            transitions[i].setOnFinished(event -> {
                String substring = welcomeMessage.substring(0, finalI);
                labelWithWelcomeMessage.setText(substring);
                labelWithWelcomeMessage.setOpacity(1);
            });
        }
        for (PauseTransition transition : transitions) {
            transition.play();
        }
        return labelWithWelcomeMessage;
    }

}