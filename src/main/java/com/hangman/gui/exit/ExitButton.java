package com.hangman.gui.exit;

import javafx.application.Platform;
import javafx.scene.control.Button;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class ExitButton {

    private static ExitButton instance;

    private ExitButton() {
    }

    public static ExitButton getInstance() {
        if (instance == null) {
            instance = new ExitButton();
        }
        return instance;
    }


    public Button exitButtonClick() {
        Button exitButton = new Button("WYJŚCIE");
        exitButton.setPrefSize(400, 50);
        exitButton.setStyle("-fx-background-radius: 3em;");
        exitButton.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        exitButton.setOnAction(event -> Platform.exit());
        return exitButton;
    }

}