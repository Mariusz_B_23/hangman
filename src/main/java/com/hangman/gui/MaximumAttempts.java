package com.hangman.gui;

public enum MaximumAttempts {

    SINGLE_PLAYER(8),

    MULTIPLAYER(8),

    LEVEL_HARD(4);

    final int quantity;

    MaximumAttempts(int quantity) {
        this.quantity = quantity;
    }

    public int getQuantity() {
        return quantity;
    }

    @Override
    public String toString() {
        return String.valueOf(quantity);
    }

}