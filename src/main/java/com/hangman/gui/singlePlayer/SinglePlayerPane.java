package com.hangman.gui.singlePlayer;

import com.hangman.gui.MaximumAttempts;
import com.hangman.utlis.SystemUtils;
import javafx.animation.PauseTransition;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Duration;

public class SinglePlayerPane {

    private final AnchorPane singlePlayerPane;
    private static final String WINNER_GIF_PATH = "gameplay_image/single/di caprio single.gif";
    private static final int WINNER_GIF_SET_FIT_WIDTH = 300;
    private static final int WINNER_GIF_SET_LAYOUT_X = 500;
    private static final int WINNER_GIF_SET_LAYOUT_Y = 420;
    private static final int PENULTIMATE_TRY = 7;
    private static final int MAXIMUM_ATTEMPTS = MaximumAttempts.SINGLE_PLAYER.getQuantity();
    private static final String PART_OF_IMAGE_PART = "gameplay_image/single/%d.jpg";
    private static final int SET_FIT_WIDTH_PART_OF_IMAGE = 550;
    private static final int SET_LAYOUT_X_PART_OF_IMAGE = 700;
    private static final int SET_LAYOUT_Y_PART_OF_IMAGE = 50;
    private static final String LOSE_GIF_PATH = "gameplay_image/single/muttley single.gif";
    private static final int LOSE_GIF_SET_FIT_WIDTH = 320;
    private static final int LOSE_GIF_SET_LAYOUT_X = 200;
    private static final int LOSE_GIF_SET_LAYOUT_Y = 420;
    private static final String FINAL_IMAGE_PATH = "gameplay_image/single/final image single.jpg";
    private static final int SET_FIT_WIDTH_FINAL_IMAGE = 430;
    private static final int SET_LAYOUT_X_FINAL_IMAGE = 700;
    private static final int SET_LAYOUT_Y_FINAL_IMAGE = 420;


    public SinglePlayerPane(Stage primaryStage, VBox vBox) {
            this.singlePlayerPane = new AnchorPane(createMainAnchorPane(vBox, primaryStage));
    }

    public AnchorPane getSinglePlayerPane() {
        return singlePlayerPane;
    }

    private static AnchorPane createMainAnchorPane(VBox vBox, Stage primaryStage) {
        AnchorPane mainAnchorPane = new AnchorPane();
        vBox.getChildren().add(mainAnchorPane);
        mainAnchorPane.setPrefSize(primaryStage.getWidth(), primaryStage.getHeight());
        Button backButtonInInfoPane = SystemUtils.createBackToMenuButton(vBox, primaryStage);
        mainAnchorPane.getChildren().add(backButtonInInfoPane);
        Label labelWithWelcomeMessage = createLabelWithWelcomeMessage();
        mainAnchorPane.getChildren().add(labelWithWelcomeMessage);
        Button continueButton = SystemUtils.createContinueButton(vBox, primaryStage);
        continueButton.setOnAction(actionEvent -> {
            Pane gamePane = SystemUtils.createGamePane(vBox, primaryStage,
                    WINNER_GIF_PATH, WINNER_GIF_SET_FIT_WIDTH,
                    WINNER_GIF_SET_LAYOUT_X, WINNER_GIF_SET_LAYOUT_Y,
                    PENULTIMATE_TRY, MAXIMUM_ATTEMPTS, PART_OF_IMAGE_PART,
                    LOSE_GIF_PATH, LOSE_GIF_SET_FIT_WIDTH,
                    LOSE_GIF_SET_LAYOUT_X, LOSE_GIF_SET_LAYOUT_Y,
                    SET_FIT_WIDTH_PART_OF_IMAGE, SET_LAYOUT_X_PART_OF_IMAGE, SET_LAYOUT_Y_PART_OF_IMAGE,
                    FINAL_IMAGE_PATH, SET_FIT_WIDTH_FINAL_IMAGE,
                    SET_LAYOUT_X_FINAL_IMAGE, SET_LAYOUT_Y_FINAL_IMAGE);
            vBox.getChildren().clear();
            vBox.getChildren().add(gamePane);
        });
        mainAnchorPane.getChildren().add(continueButton);
        return mainAnchorPane;
    }

    private static Label createLabelWithWelcomeMessage() {
        String welcomeMessage = """
                Witaj w trybie jednego gracza.
                W tej rozgrywce wyraz jest losowany ze specjalnej listy.

                Możesz pomylić się maksymalnie (aż) 7 razy!
                Jeśli pomylisz się 8 raz ...
                ... to zawiśniesz :)

                Rozpocznijmy zatem tą pasjonującą rozgrywkę ...""";
        Label labelWithWelcomeMessage = SystemUtils.welcomeMessageLabel(welcomeMessage);
        int welcomeMessageLength = welcomeMessage.length();
        PauseTransition[] transitions = new PauseTransition[welcomeMessageLength];
        for (int i = 0; i < welcomeMessageLength; i++) {
            transitions[i] = new PauseTransition(Duration.seconds(i * 0.06));
            final int finalI = i + 1;
            transitions[i].setOnFinished(event -> {
                String substring = welcomeMessage.substring(0, finalI);
                labelWithWelcomeMessage.setText(substring);
                labelWithWelcomeMessage.setOpacity(1);
            });
        }
        for (PauseTransition transition : transitions) {
            transition.play();
        }
        return labelWithWelcomeMessage;
    }

}