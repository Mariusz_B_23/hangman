package com.hangman.gui;

import com.hangman.utlis.SystemUtils;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class PrimaryStage {


    public void initialize(Stage primaryStage) {
        MainVboxView mainVboxView = new MainVboxView(primaryStage);
        Scene scene = new Scene(mainVboxView.getMainBoxView());
        primaryStage.setTitle(SystemUtils.GAME_NAME);
        primaryStage.setFullScreen(true);
        primaryStage.setFullScreenExitHint("");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

}