package com.hangman.gui;

import com.hangman.gui.exit.ExitButton;
import com.hangman.gui.levelHard.LevelHardPane;
import com.hangman.gui.multiplayer.MultiPlayerPane;
import com.hangman.gui.singlePlayer.SinglePlayerPane;
import com.hangman.utlis.SystemUtils;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

public class MainVboxView {

    private final VBox mainVBox;


    public MainVboxView(Stage primaryStage) {
        this.mainVBox = new VBox();
        Label bigHangmanLabel = createBigHangmanLabel();
        this.mainVBox.getChildren().add(bigHangmanLabel);
        this.mainVBox.setSpacing(30);
        Button singlePlayer = singlePlayerClickButton(mainVBox, primaryStage);
        Button multiPlayer = multiPlayerClickButton(mainVBox, primaryStage);
        Button levelHard = levelHardClickButton(mainVBox, primaryStage);
        mainVBox.setAlignment(Pos.CENTER);
        this.mainVBox.getChildren().addAll(singlePlayer, multiPlayer, levelHard, ExitButton.getInstance().exitButtonClick());
    }

    public Pane getMainBoxView() {
        return mainVBox;
    }

    private static Label createBigHangmanLabel() {
        Label label = new Label(SystemUtils.GAME_NAME);
        label.setFont(Font.font("Verdana", FontWeight.BOLD, FontPosture.ITALIC, 100));
        label.setRotate(5);
        label.setStyle("-fx-border-color: black; -fx-border-width: 3px;");
        label.setPadding(new Insets(20, 20, 20, 20));
        return label;
    }

    public Button singlePlayerClickButton(VBox vBox, Stage primaryStage) {
        Button singlePlayerButton = SystemUtils.createGameButton(SystemUtils.SINGLE_GAME_MODE);
        singlePlayerButton.setOnAction(e -> {
            vBox.getChildren().clear();
            SinglePlayerPane singlePlayerPane = new SinglePlayerPane(primaryStage, vBox);
            vBox.getChildren().add(singlePlayerPane.getSinglePlayerPane());
        });
        return singlePlayerButton;
    }

    public Button multiPlayerClickButton(VBox vBox, Stage primaryStage) {
        Button multiPlayerButton = SystemUtils.createGameButton(SystemUtils.MULTI_GAME_MODE);
        multiPlayerButton.setOnAction(e -> {
            vBox.getChildren().clear();
            MultiPlayerPane multiPlayerPane = new MultiPlayerPane(primaryStage, vBox);
            vBox.getChildren().add(multiPlayerPane.getMultiPlayerPane());
        });
        return multiPlayerButton;
    }

    public Button levelHardClickButton(VBox vBox, Stage primaryStage) {
        Button levelHardButton = SystemUtils.createGameButton(SystemUtils.HARD_GAME_MODE);
        levelHardButton.setOnAction(e -> {
            vBox.getChildren().clear();
            LevelHardPane levelHardPane = new LevelHardPane(primaryStage, vBox);
            vBox.getChildren().add(levelHardPane.getLevelHardPane());
        });
        return levelHardButton;
    }


}
