package com.hangman.gui.multiplayer;

import com.hangman.gui.MaximumAttempts;
import com.hangman.utlis.SystemUtils;
import javafx.animation.KeyFrame;
import javafx.animation.PauseTransition;
import javafx.animation.Timeline;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import javafx.util.Duration;

public class MultiPlayerPane {

    private final AnchorPane multiPlayerPane;
    private static final String ALLOWED_CHARS_PATTERN = "([A-Za-zĄąĆćĘęŁłŃńÓóŚśŹźŻż]+ *)+";
    private static final String WINNER_GIF_PATH = "gameplay_image/multi/jim carrey multi.gif";
    private static final int WINNER_GIF_SET_FIT_WIDTH = 320;
    private static final int WINNER_GIF_SET_LAYOUT_X = 500;
    private static final int WINNER_GIF_SET_LAYOUT_Y = 420;
    private static final int PENULTIMATE_TRY = 7;
    private static final int MAXIMUM_ATTEMPTS = MaximumAttempts.MULTIPLAYER.getQuantity();
    private static final String PART_OF_IMAGE_PART = "gameplay_image/multi/%d.jpg";
    private static final int SET_FIT_WIDTH_PART_OF_IMAGE = 450;
    private static final int SET_LAYOUT_X_PART_OF_IMAGE = 700;
    private static final int SET_LAYOUT_Y_PART_OF_IMAGE = 50;
    private static final String LOSE_GIF_PATH = "gameplay_image/multi/facepalm multi.gif";
    private static final int LOSE_GIF_SET_FIT_WIDTH = 470;
    private static final int LOSE_GIF_SET_LAYOUT_X = 200;
    private static final int LOSE_GIF_SET_LAYOUT_Y = 420;
    private static final String FINAL_IMAGE_PATH = "gameplay_image/multi/final image multi.jpg";
    private static final int SET_FIT_WIDTH_FINAL_IMAGE = 363;
    private static final int SET_LAYOUT_X_FINAL_IMAGE = 750;
    private static final int SET_LAYOUT_Y_FINAL_IMAGE = 420;


    public MultiPlayerPane(Stage primaryStage, VBox vBox) {
        this.multiPlayerPane = new AnchorPane(createMainAnchorPane(vBox, primaryStage));
    }

    public AnchorPane getMultiPlayerPane() {
        return multiPlayerPane;
    }

    private static AnchorPane createMainAnchorPane(VBox vBox, Stage primaryStage) {
        AnchorPane mainAnchorPane = new AnchorPane();
        vBox.getChildren().add(mainAnchorPane);
        mainAnchorPane.setPrefSize(primaryStage.getWidth(), primaryStage.getHeight());
        Button backButtonInInfoPane = SystemUtils.createBackToMenuButton(vBox, primaryStage);
        mainAnchorPane.getChildren().add(backButtonInInfoPane);
        Label labelWithWelcomeMessage = createLabelWithWelcomeMessage();
        mainAnchorPane.getChildren().add(labelWithWelcomeMessage);
        Button continueButton = createEnterPasswordButton(vBox, primaryStage);
        continueButton.setOnAction(actionEvent -> {
            Pane gamePane = createUserEntersPasswordPane(vBox, primaryStage);
            vBox.getChildren().clear();
            vBox.getChildren().add(gamePane);
        });
        mainAnchorPane.getChildren().add(continueButton);
        return mainAnchorPane;
    }

    private static Label createLabelWithWelcomeMessage() {
        String welcomeMessage = """
                Witaj w trybie dwóch graczy.
                Wyraz podawany jest przez Twojego przeciwnika.
                                
                Możesz pomylić się maksymalnie (aż) 7 razy!
                                
                Jeśli pomylisz się 8 raz ...
                ... to zawiśniesz :)
                                
                Rozpocznijmy zatem tą pasjonującą rozgrywkę...""";
        Label labelWithWelcomeMessage = SystemUtils.welcomeMessageLabel(welcomeMessage);
        int welcomeMessageLength = welcomeMessage.length();
        PauseTransition[] transitions = new PauseTransition[welcomeMessageLength];
        for (int i = 0; i < welcomeMessageLength; i++) {
            transitions[i] = new PauseTransition(Duration.seconds(i * 0.06));
            final int finalI = i + 1;
            transitions[i].setOnFinished(event -> {
                String substring = welcomeMessage.substring(0, finalI);
                labelWithWelcomeMessage.setText(substring);
                labelWithWelcomeMessage.setOpacity(1);
            });
        }
        for (PauseTransition transition : transitions) {
            transition.play();
        }
        return labelWithWelcomeMessage;
    }

    public static Button createEnterPasswordButton(VBox vBox, Stage primaryStage) {
        Button continueButton = new Button("WPROWADŹ HASŁO");
        continueButton.setPrefSize(150, 30);
        continueButton.setStyle("-fx-background-radius: 3em;");
        continueButton.setFont(Font.font("Arial", FontWeight.BOLD, 12));
        continueButton.setLayoutX(primaryStage.getWidth() - continueButton.getPrefWidth() - 50);
        continueButton.setLayoutY(primaryStage.getHeight() - continueButton.getHeight() - 50);
        continueButton.setOnAction(event -> vBox.getChildren().clear());
        return continueButton;
    }

    private static Pane createUserEntersPasswordPane(VBox vBox, Stage primaryStage) {
        Pane enterPasswordPane = new Pane();
        Label hangmanLabel = createBigHangmanLabelForEnterPassword();
        Label messageForUser = createMessageForUser();
        Button backButton = SystemUtils.createBackToMenuButton(vBox, primaryStage);
        Button continueButton = SystemUtils.createContinueButton(vBox, primaryStage);
        TextField userGivePassword = userGivePasswordForSecondUser();
        final int maximumPasswordLength = 30;
        continueButton.setOnAction(event -> {
            if (!(userGivePassword.getText().trim().isEmpty())
                    && userGivePassword.getText().length() <= maximumPasswordLength
                    && userGivePassword.getText().trim().matches(ALLOWED_CHARS_PATTERN)) {
                String userGivePasswordAsString = userGivePassword.getText().toUpperCase().trim();
                System.out.printf("Hasło ->   %s\n", userGivePasswordAsString);
                Pane gamePane = SystemUtils.createGamePane(vBox, primaryStage, userGivePasswordAsString,
                        WINNER_GIF_PATH, WINNER_GIF_SET_FIT_WIDTH,
                        WINNER_GIF_SET_LAYOUT_X, WINNER_GIF_SET_LAYOUT_Y,
                        PENULTIMATE_TRY, MAXIMUM_ATTEMPTS, PART_OF_IMAGE_PART,
                        LOSE_GIF_PATH, LOSE_GIF_SET_FIT_WIDTH,
                        LOSE_GIF_SET_LAYOUT_X, LOSE_GIF_SET_LAYOUT_Y,
                        SET_FIT_WIDTH_PART_OF_IMAGE, SET_LAYOUT_X_PART_OF_IMAGE, SET_LAYOUT_Y_PART_OF_IMAGE,
                        FINAL_IMAGE_PATH, SET_FIT_WIDTH_FINAL_IMAGE,
                        SET_LAYOUT_X_FINAL_IMAGE, SET_LAYOUT_Y_FINAL_IMAGE);
                vBox.getChildren().clear();
                vBox.getChildren().add(gamePane);
            } else {
                createUserEntersIncorrectPasswordPropositionLabel(enterPasswordPane);
            }
        });
        vBox.getChildren().clear();
        vBox.getChildren().add(enterPasswordPane);
        enterPasswordPane.setPrefSize(primaryStage.getWidth(), primaryStage.getHeight());
        enterPasswordPane.getChildren()
                .addAll(hangmanLabel, messageForUser, userGivePassword,
                        backButton, continueButton);
        return enterPasswordPane;
    }

    private static Label createBigHangmanLabelForEnterPassword() {
        Label hangmanLabel = new Label(SystemUtils.GAME_NAME);
        hangmanLabel.setFont(Font.font("Verdana", FontWeight.BOLD, FontPosture.ITALIC, 100));
        hangmanLabel.setLayoutX(370);
        hangmanLabel.setLayoutY(150);
        hangmanLabel.setStyle("-fx-border-color: black; -fx-border-width: 3px;");
        hangmanLabel.setPadding(new Insets(20, 20, 20, 20));
        return hangmanLabel;
    }

    private static Label createMessageForUser() {
        String messageForUserAboutHisPassword =
                """
                        Podaj hasło, które będzie musiał zgadnąć Twój przeciwnik.
                        Hasło może składać się z maksymalnie 30 znaków (litery i spacje).
                        W haśle nie mogą znajdować się cyfry, ani inne znaki specjalne.
                        """;
        Label messageForUser = new Label(messageForUserAboutHisPassword);
        messageForUser.setFont(Font.font("Ubuntu", FontWeight.BOLD, 30));
        messageForUser.setLayoutX(215);
        messageForUser.setLayoutY(350);
        messageForUser.setTextAlignment(TextAlignment.CENTER);
        messageForUser.setStyle("-fx-border-color: black; -fx-border-width: 2px;");
        messageForUser.setPadding(new Insets(10, 10, 10, 10));
        return messageForUser;
    }

    private static TextField userGivePasswordForSecondUser() {
        TextField userPassword = new TextField();
        userPassword.setLayoutX(205);
        userPassword.setLayoutY(540);
        userPassword.setStyle("-fx-border-color: black; -fx-border-width: 2px;");
        userPassword.setPadding(new Insets(5, 10, 5, 10));
        userPassword.setFont(Font.font("Comic Sans MS", FontWeight.BOLD, 30));
        userPassword.setPrefSize(970, 75);
        userPassword.setPromptText("Tutaj wprowadź hasło...");
        userPassword.setStyle("-fx-prompt-text-fill: gray;");
        return userPassword;
    }

    private static void createUserEntersIncorrectPasswordPropositionLabel(Pane enterPasswordPane) {
        Label incorrectPasswordPropositionLabel = new Label("HASŁO JEST NIEPRAWIDŁOWE !!!");
        incorrectPasswordPropositionLabel.setLayoutX(410);
        incorrectPasswordPropositionLabel.setLayoutY(640);
        incorrectPasswordPropositionLabel.setTextFill(Color.RED);
        incorrectPasswordPropositionLabel.setFont(Font.font("Comic Sans MS", FontWeight.BOLD, 30));
        enterPasswordPane.getChildren().add(incorrectPasswordPropositionLabel);
        Timeline timeline = new Timeline(new KeyFrame(Duration.seconds(1),
                e -> enterPasswordPane.getChildren().remove(incorrectPasswordPropositionLabel)));
        timeline.play();
    }

}