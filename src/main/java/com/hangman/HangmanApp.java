package com.hangman;

import com.hangman.gui.PrimaryStage;
import javafx.application.Application;
import javafx.stage.Stage;


public class HangmanApp extends Application {


    public static void main(String[] args) {

//        HangmanManager hangmanManager = HangmanManager.getInstance();
//        hangmanManager.messageWelcome();
//        int userChoose = hangmanManager.getUserChoose();
//        hangmanManager.systemGame(userChoose);

        try {
            Application.launch(args);
        } catch (Exception e) {
            System.out.println("Błąd " + e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void start(Stage primaryStage) {
        System.out.println("Uruchamiam aplikację.");
        PrimaryStage primary = new PrimaryStage();
        primary.initialize(primaryStage);
    }

    @Override
    public void stop() {
        System.out.println("Wychodzę z aplikacji.");
    }

}