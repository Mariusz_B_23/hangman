package com.hangman.domain;

import com.hangman.utlis.SystemUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class HangmanManager {

    public static final Scanner KEYBOARD = new Scanner(System.in);
    public static Scanner scanner;
    public static final Random RANDOM = new Random();
    private static final List<Character> PLAYER_GUESSES = new ArrayList<>();
    private static final List<String> LETTERS_WHICH_WERE = new ArrayList<>();
    private static final Set<String> ALPHABET = createSetWithAlphabet();
    private static final SinglePlayer SINGLE_PLAYER = new SinglePlayer();
    private static final Multiplayer MULTIPLAYER = new Multiplayer();
    private static final LevelHard LEVEL_HARD = new LevelHard();

    private static HangmanManager instance = null;

    private HangmanManager() {
    }

    public static HangmanManager getInstance() {
        if (instance == null) {
            instance = new HangmanManager();
        }
        return instance;
    }

    public void messageWelcome() {
        System.out.println("""
                                
                Witaj w grze Wisielec
                Żeby rozpocząć grę, wybierz jedną z opcji:
                - wybierz 1 -> JEDEN GRACZ
                - wybierz 2 -> DWÓCH GRACZY
                - wybierz 3 -> DO TRZECH RAZY SZTUKA - LEVEL HARD :)
                lub:
                - wybierz 0 -> WYJŚCIE Z GRY
                                
                Proszę podaj liczbę
                """);
    }

    public int getUserChoose() {
        String userChoose;
        String systemRegex = "[0-3]";
        do {
            userChoose = KEYBOARD.nextLine();
            if (!userChoose.matches(systemRegex)) {
                System.out.println("Musisz podać liczbę od 0 do 3!");
            }
        } while (!userChoose.matches(systemRegex));

        return Integer.parseInt(userChoose);
    }

    public void systemGame(int userSelection) {
        if (userSelection <= -1) {
            throw new IllegalArgumentException("Podana liczba nie może być mniejsza od zera.");
        }
        switch (userSelection) {
            case 0 -> System.out.println("Wychodzę z gry. Zapraszamy ponownie!");
            case 1 -> singlePlayer();
            case 2 -> multiplayer();
            case 3 -> levelHard();
            default -> System.out.println("NIE MA TAKIEJ OPCJI");
        }
    }

    public void singlePlayer() {
        SINGLE_PLAYER.instructionWelcome();
        List<String> words = downloadDataFile();
        String word = informationAboutWordFromList(words);
        SINGLE_PLAYER.gameplay(word);
    }

    private static String informationAboutWordFromList(List<String> words) {
        if (words.isEmpty()) {
            throw new IllegalArgumentException("Lista wyrazów nie może być pusta!");
        }
        String word = words.get(RANDOM.nextInt(words.size()));
        System.out.println("Wyraz składa się z " + word.replaceAll(" ", "").length() + " liter." + "\n");
        return word;
    }

    public void multiplayer() {
        MULTIPLAYER.instructionWelcome();
        String word = informationAboutWordFromUser();
        MULTIPLAYER.gameplay(word);
    }

    private static String informationAboutWordFromUser() {
        System.out.println("\n" + "Proszę wprowadź słowo które przeciwnik musi odgadnąć");
        String word = KEYBOARD.nextLine().toUpperCase();
        System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
        System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
        System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
        System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
        System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
        System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
        System.out.println("Hasło zostało wprowadzone. Powodzenia!!!" + "\n");
        System.out.println("Wyraz składa się z " + word.replaceAll(" ", "").length() + " liter." + "\n");
        return word;
    }

    public void levelHard() {
        LEVEL_HARD.instructionWelcome();
        int userStartChoice = LevelHard.levelHardSelectionBeginning();
        String word = LevelHard.selectionGameMode(userStartChoice);
        LEVEL_HARD.gameplay(word);
    }

    public static List<String> downloadDataFile() {
        try {
            scanner = new Scanner(new File(SystemUtils.FILE_PATH_WITH_PASSWORDS));
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
        List<String> words = new ArrayList<>();
        while (scanner.hasNext()) {
            words.add(scanner.nextLine());
        }
        return words;
    }

    public static boolean getPlayerGuess(String word) {
        if (word.isEmpty() || word.isBlank()) {
            throw new IllegalArgumentException("Słowo nie może być puste!");
        }
        if (LETTERS_WHICH_WERE.isEmpty()) {
            System.out.println();
        } else {
            System.out.println("\n" + "Litery które już podałeś");
            System.out.println(LETTERS_WHICH_WERE);
        }
        System.out.println("\n" + "Dostępne litery");
        System.out.println(ALPHABET);
        System.out.println("\n" + "Proszę, podaj literę:");
        String letterGuess;
        String regex = "[a-zA-ZżźćńółęąśŻŹĆĄŚĘŁÓŃ]";
        do {
            letterGuess = KEYBOARD.nextLine().toUpperCase();
            if (!letterGuess.matches(regex)) {
                System.out.println("Musisz podać JEDNĄ LITERĘ. Spróbuj jeszcze raz");
            }
            if (LETTERS_WHICH_WERE.contains(letterGuess)) {
                System.out.println("Podałeś już tą literę! Podaj nową!");
            }
        } while (!letterGuess.matches(regex) || LETTERS_WHICH_WERE.contains(letterGuess));
        if (word.contains(" ")) {
            PLAYER_GUESSES.add(' ');
        }
        PLAYER_GUESSES.add(letterGuess.charAt(0));
        ALPHABET.remove(letterGuess);
        LETTERS_WHICH_WERE.add(letterGuess);
        return !word.contains(letterGuess);
    }

    public static boolean wordService(String word) {
        if (word.isEmpty() || word.isBlank()) {
            throw new IllegalArgumentException("Słowo nie może być puste!");
        }
        int correctCount = 0;
        for (int i = 0; i < word.length(); i++) {
            if (PLAYER_GUESSES.contains(word.charAt(i))) {
                System.out.print(word.charAt(i));
                correctCount++;
            } else {
                System.out.print("-");
            }
        }
        System.out.println();
        return (word.length() == correctCount);
    }

    private static Set<String> createSetWithAlphabet() {
        String[] array = new String[]{"A", "Ą", "B", "C", "Ć", "D", "E", "Ę", "F", "G", "H", "I", "J", "K", "L",
                "Ł", "M", "N", "Ń", "O", "Ó", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "Ź", "Ż"};
        return new LinkedHashSet<>(Arrays.asList(array));
    }

}