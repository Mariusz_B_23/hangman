package com.hangman.domain;

public interface Game {


    void instructionWelcome();

    void gameplay(String word);

    void printHangedMan(int wrongCount);

}