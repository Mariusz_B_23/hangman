package com.hangman.domain;

public class Multiplayer implements Game {


    @Override
    public void instructionWelcome() {
        System.out.println("""
                                
                Witaj w trybie dwóch graczy. W tej rozgrywce wyraz jest podawany przez Twojego przeciwnika.
                Możesz pomylić się maksymalnie (aż) 7 razy!
                Jeśli pomylisz się 8 raz ... to zawiśniesz :)
                Rozpocznijmy zatem tą pasjonującą rozgrywkę...
                                
                """);
    }

    @Override
    public void gameplay(String word) {
        if (word.isEmpty() || word.isBlank()) {
            throw new IllegalArgumentException("Słowo nie może być puste!");
        }
        int wrongCount = 0;
        while (true) {
            printHangedMan(wrongCount);
            if (wrongCount >= 8) {
                System.out.println("Przegrałeś! Dyndasz ha ha ha :)");
                System.out.println("Prawidłowe hasło to: " + word);
                break;
            }
            HangmanManager.wordService(word);
            if (HangmanManager.getPlayerGuess(word)) {
                wrongCount++;
            }
            if (HangmanManager.wordService(word)) {
                System.out.println("Brawo! Wygrałeś! :)");
                break;
            }
            System.out.println("\n" + "Proszę wprowadź swoją propozycję hasła:");
            if (HangmanManager.KEYBOARD.nextLine().toUpperCase().equals(word)) {
                System.out.println("Brawo! Wygrałeś! :)");
                break;
            } else {
                System.out.println("-------------------------------------------------------------------------------------------------------------");
                System.out.println("""

                        Niestety, to nie jest prawidłowe hasło.
                        """);
                System.out.println("Liczba pomyłek wynosi: " + wrongCount + "\n");
                System.out.println("-------------------------------------------------------------------------------------------------------------" + "\n");
                if (wrongCount == 7) {
                    System.out.println("""
                            To była Twoja siódma pomyłka! Jeszcze jedna i DYNDASZ! Czujesz tą presję?... :)
                                                        
                            -------------------------------------------------------------------------------------------------------------
                                                        
                            """);
                }
            }
        }
    }

    @Override
    public void printHangedMan(int wrongCount) {
        if (wrongCount <= -1) {
            throw new IllegalArgumentException("Podana liczba nie może być mniejsza od zera.");
        }
        if (wrongCount >= 4) {
            System.out.print("""
                        []###########
                        []         |
                        []         |
                    """);
        }
        if (wrongCount >= 5) {
            System.out.println("    []         O");
        }
        if (wrongCount >= 6) {
            System.out.println("    []        \\ /");
        }
        if (wrongCount >= 7) {
            System.out.println("    []         |");
        }
        if (wrongCount >= 8) {
            System.out.println("    []        / \\");
        }
        if (wrongCount >= 3) {
            System.out.println("    []");
        }
        if (wrongCount >= 2) {
            System.out.println("    []");
        }
        if (wrongCount >= 1) {
            System.out.println("""
                        []
                    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                    -------------------------------------------------------------------------------------------------------------
                                        
                    """);
        }
    }

}