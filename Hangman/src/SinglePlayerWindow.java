import javax.swing.*;

public class SinglePlayerWindow {
    private final HangmanManager hangmanManager = HangmanManager.getInstance();


    public SinglePlayerWindow() {
        JFrame frame = new JFrame("SINGLE PLAYER");
        JPanel singlePlayerPanel = new JPanel();
        frame.setUndecorated(false);
        frame.setResizable(true);
        frame.add(singlePlayerPanel);
        frame.pack();
        frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        frame.setLocation(0, 0);
        frame.setVisible(true);
    }

}