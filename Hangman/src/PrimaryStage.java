

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;


public class PrimaryStage {

    private final HangmanManager hangmanManager = HangmanManager.getInstance();
    private final JFrame mainFrame = new JFrame();
    private final JPanel mainPanel = new JPanel();

    public void run() {
        mainFrame.setTitle("HANGMAN");
        mainFrame.setUndecorated(false);
        mainFrame.setResizable(true);
        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
        mainPanel.setBorder(BorderFactory.createEmptyBorder(200, 200, 200, 200));
        mainFrame.add(mainPanel);
        addLabel(mainPanel);
        addButtons(mainPanel, "SINGLE PLAYER", "TWO PLAYERS", "LEVEL HARD", "EXIT");
        mainFrame.getContentPane().add(mainPanel);
        addWindowListener(mainFrame);
        mainFrame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        mainFrame.setLocation(0, 0);
        mainFrame.setVisible(true);
    }


    private void addButtons(Container container, String... texts) {
        for (String text : texts) {
            JButton button = new JButton(text);
            button.setMaximumSize(new Dimension(500, 100));
            button.setAlignmentX(Component.CENTER_ALIGNMENT);
            button.setFont(new Font("Arial", Font.BOLD, 24));
            button.addActionListener(e -> {
                mainPanel.removeAll();
                mainPanel.repaint();
                switch (text) {
                    case "SINGLE PLAYER" -> new SinglePlayerWindow();
                    case "TWO PLAYERS" -> new MultiplayerWindow();
                    case "LEVEL HARD" -> new LevelHardWindow();
                    case "EXIT" -> System.exit(0);
                }
            });
            container.add(button);
            container.add(Box.createRigidArea(new Dimension(0, 50)));
        }
    }



    private void addLabel(Container container) {
        JLabel label = new JLabel("HANGMAN", JLabel.CENTER);
        label.setAlignmentX(Component.CENTER_ALIGNMENT);
        label.setFont(new Font("Arial", Font.BOLD, 100));
        label.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
        container.add(label);
        container.add(Box.createVerticalStrut(50));
    }

    private void addWindowListener(JFrame frame) {
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                frame.dispose();
            }

            @Override
            public void windowClosed(WindowEvent e) {
                System.exit(0);
            }
        });
    }

}