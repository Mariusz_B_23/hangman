import java.util.List;

public class LevelHard implements Game {

    @Override
    public void instructionWelcome() {

        System.out.println("""
                                
                Witaj w grze o specjalnym poziomie trudności.
                Podczas odgadywania słowa możesz pomylić się maksymalnie trzy razy!
                Jeśli chcesz:
                - wylosować hasło z pliku wybierz 1
                - jeśli ma je podać drugi gracz wybierz 2
                                
                """);

    }

    @Override
    public void gameplay(String word) {

        if (word.isEmpty() || word.isBlank()) {
            throw new IllegalArgumentException("Słowo nie może być puste!");
        }

        int wrongCount = 0;
        while (true) {

           printHangedMan(wrongCount);

            if (wrongCount >= 3) {
                System.out.println("Przegrałeś! Dyndasz ha ha ha :)");
                System.out.println("Prawidłowe hasło to: " + word);
                break;
            }
            HangmanManager.wordService(word);
            if (HangmanManager.getPlayerGuess(word)) {
                wrongCount++;
            }

            if (HangmanManager.wordService(word)) {
                System.out.println("Brawo! Wygrałeś! :)");
                break;
            }

            System.out.println("\n" + "Proszę wprowadź swoją propozycję hasła:");
            if (HangmanManager.KEYBOARD.nextLine().toUpperCase().equals(word)) {
                System.out.println("Brawo! Wygrałeś! :)");
                break;
            } else {
                System.out.println("-------------------------------------------------------------------------------------------------------------");
                System.out.println("""

                        Niestety, to nie jest prawidłowe hasło.
                        """);
                System.out.println("Liczba pomyłek wynosi: " + wrongCount + "\n");
                System.out.println("-------------------------------------------------------------------------------------------------------------" + "\n");
                if (wrongCount == 2) {
                    System.out.println("""
                            To była Twoja ostatnia pomyłka! Jeszcze jedna i DYNDASZ! Czujesz tą presję?... :)
                                                        
                            -------------------------------------------------------------------------------------------------------------
                                                        
                            """);
                }
            }
        }

    }

    @Override
    public void printHangedMan(int wrongCount) {

        if (wrongCount <= -1) {
            throw new IllegalArgumentException("Podana liczba nie może być mniejsza od zera.");
        }

        if (wrongCount >= 2) {
            System.out.print("""
                            []###########
                            []         |
                            []         |
                    """);

        }

        if (wrongCount >= 3) {
            System.out.print("""
                            []         O
                            []        \\ /
                            []         |
                            []        / \\
                    """);
        }

        if (wrongCount >= 1) {
            System.out.println("""
                            []
                            []
                            []
                        @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                        -------------------------------------------------------------------------------------------------------------
                          
                    """);

        }
    }


    public static int levelHardSelectionBeginning() {

        String userStartChoice;
        String regex = "[1-2]";

        do {
            userStartChoice = HangmanManager.KEYBOARD.nextLine();
            if (!userStartChoice.matches(regex)) {
                System.out.println("Musisz podać liczbę 1 albo 2!");
            }
        } while (!userStartChoice.matches(regex));

        System.out.println();

        return Integer.parseInt(userStartChoice);

    }


    public static String selectionGameMode(int userStartChoice) {

        String word = "";
        if (userStartChoice == 1) {
            word = levelHardFirstOption();
        } else if (userStartChoice == 2) {
            word = levelHardSecondOption();
        } else {
            System.out.println("Musisz podać cyfrę 1 albo 2!");
        }
        return word;
    }


    private static String levelHardFirstOption() {

        List<String> words = HangmanManager.downloadDataFile();

        while (HangmanManager.scanner.hasNext()) {
            words.add(HangmanManager.scanner.nextLine());
        }

        String word = words.get(HangmanManager.RANDOM.nextInt(words.size()));
        System.out.println("Wyraz składa się z " + word.replaceAll(" ", "").length() + " liter." + "\n");

        return word;

    }


    private static String levelHardSecondOption() {

        System.out.println("Proszę wprowadź słowo które przeciwnik musi odgadnąć");
        String word = HangmanManager.KEYBOARD.nextLine().toUpperCase();
        System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
        System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
        System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
        System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
        System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
        System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
        System.out.println("Hasło zostało wprowadzone. Powodzenia!!!" + "\n");
        System.out.println("Wyraz składa się z " + word.replaceAll(" ", "").length() + " liter." + "\n");

        return word;

    }


}
