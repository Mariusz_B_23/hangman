import javax.swing.*;


public class MultiplayerWindow {

    public MultiplayerWindow() {
        JFrame frame = new JFrame("MULTIPLAYER");
        JPanel singlePlayerPanel = new JPanel();
        frame.setUndecorated(false);
        frame.setResizable(true);
        frame.add(singlePlayerPanel);
        frame.pack();
        frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        frame.setLocation(0, 0);
        frame.setVisible(true);
    }

}