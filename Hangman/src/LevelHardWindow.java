import javax.swing.*;

public class LevelHardWindow {

    public LevelHardWindow() {
        JFrame frame = new JFrame("LEVEL HARD");
        JPanel singlePlayerPanel = new JPanel();
        frame.setUndecorated(false);
        frame.setResizable(true);
        frame.add(singlePlayerPanel);
        frame.pack();
        frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        frame.setLocation(0, 0);
        frame.setVisible(true);
    }
}